%! Author = FIELDSC
%! Date = 2022-08-15

% Preamble
\documentclass[10pt]{article}

% Packages
\usepackage{fancyhdr} % header settings
\usepackage[framemethod=tikz]{mdframed} % text box frames
%   Principal MPD frame type
\mdfdefinestyle{myframe}{
      frametitlebackgroundcolor=black!15,
      frametitlerule=true,
      roundcorner=10pt,
      middlelinewidth=1pt,
      innermargin=0.5cm,
      outermargin=0.5cm,
      innerleftmargin=0.5cm,
      innerrightmargin=0.5cm,
      innertopmargin=\topskip,
      innerbottommargin=\topskip,
      frametitle={CAVEATS}
    }
\newmdenv[style=myframe]{caveat}
\usepackage{hyperref}
\definecolor{darkcustomblue}{RGB}{0,102,153}
\definecolor{bluenew}{RGB}{205,236,247}
\hypersetup{
    colorlinks,
    linkcolor={black},
    citecolor={blue!50!black},
    urlcolor={black}
}
\usepackage[sfdefault]{inter} %% Option 'sfdefault' only if 'inter' is to be
% the base font of the document
\usepackage[T1]{fontenc}
\usepackage{tgbonum}
% Bar chart drawing library 
\usepackage{pgfplots} 
\usepackage{titlesec}
% line underneath section
\titleformat{\section}
  {\normalfont\large\bfseries}{\thesection}{1em}{}[\color{darkgray}{\titlerule[0.8pt]}]
% subsection headers
\titleformat{\subsection}{\normalfont\bfseries}{\thesubsection}{1em}{}[\color{gray}\hrule]
% subsubsection headers
\titleformat{\subsubsection}{\normalfont}{\thesubsubsection}{1em}{}[\color{lightgray}\hrule]
\renewcommand\UrlFont{\color{darkcustomblue}\sffamily}

\usepackage{pdftexcmds}
\usepackage[legalpaper, portrait, margin=.75in]{geometry}

\setcounter{secnumdepth}{0}

% set parameters for header
\pagestyle{fancy}
\fancyhf{}
\fancyhead[LE,RO]{\VAR{lheader}}
\fancyhead[RE,LO]{\VAR{rheader}}
\fancyfoot[LE,RO]{\thepage}
 % line width header and footer
\renewcommand{\headrulewidth}{1pt}
\renewcommand{\footrulewidth}{1pt}
% variables for title, author, date
\title{\VAR{title}}
\date{\VAR{date}}
\author{\VAR{author}}

% Document
\begin{document}
\maketitle

\begin{caveat}
  \emph{The tool that creates this document only reports on spatial data layers in the following formats:}
  \BLOCK{for format in format_list}
    \begin{itemize}
      \item[--]\VAR{format}
    \end{itemize}
  \BLOCK{ endfor }
  \emph{French text is stripped from metadata if it exists. Characters with accents in embedded metadata are not 
  consistently retained.}
\end{caveat}

\tableofcontents
\pagebreak

\section{LAYERS (\VAR{num_layers.total})}
\emph{Spatial layers are listed here with hyperlinks to the Metadata section of the document. At the very least, the metadata 
section will contain the file size and last modified date of the layer (or workspace). If there is embedded metadata in 
the spatial layer, it will be included as well. For each subdirectory that the script is processing, the layers are listed 
alphabetically. Therefore, the entire list is sorted by its location first and then alphabetically.}

\begin{figure}[!h]
  \centering
  \begin{tikzpicture}
    \begin{axis} [title=\textbf{Layers by Spatial Data Type},
                  ybar,
                  height=10cm,
                  width=14cm,
                  bar width=18pt,
                  xlabel=\textbf{Data Type},
                  ylabel=\textbf{Count},
                  ymin=0,
                  xtick={0,1,2,3,4},
                  xticklabels={Point (\VAR{num_layers.point_total}),
                               Multipoint (\VAR{num_layers.multipoint_total}),
                               Line (\VAR{num_layers.line_total}),
                               Polygon (\VAR{num_layers.polygon_total}),
                               Raster (\VAR{num_layers.raster_total})}]
      \addplot coordinates {
          (0,\VAR{num_layers.point_total})
          (1,\VAR{num_layers.multipoint_total})
          (2,\VAR{num_layers.line_total})
          (3,\VAR{num_layers.polygon_total})
          (4,\VAR{num_layers.raster_total})
      };
    \end{axis}
  \end{tikzpicture}
\end{figure}

\BLOCK{for k, v in filepath_dictionary.items()}
  \BLOCK{for layer in v}
    \begin{itemize}
      \item\hyperlink{\VAR{layer.name}}{\nolinkurl{\VAR{layer.name}}}
      \ifnum\VAR{layer.has_sublayers}=1
        \begin{itemize}
          \BLOCK{for lyr in layer.layers}
              \item[--]\nolinkurl{\VAR{lyr}}
          \BLOCK{ endfor }
        \end{itemize}
      \fi
    \end{itemize}
  \BLOCK{ endfor }
\BLOCK{ endfor }

\pagebreak

\section{METADATA}
\emph{This section contains embedded metadata such as the title, summary, description, and tags if they are included in the 
spatial layer. Each layer (or workspace such as a File Geodatabase or Geopackage) should show the file size and modified date. 
Note that the 'Date Modified' may be updated on the filesystem from the script touching the file.}
\BLOCK{for k, v in filepath_dictionary.items()}
  \subsection*{\url{\VAR{k}}}
  \BLOCK{for layer in v}
    \hypertarget{\VAR{layer.name}}{\nolinkurl{\VAR{layer.name}}}  (\VAR{layer.data_types[0]})
    \ifnum\VAR{layer.has_spatial_meta}=1
      \begin{mdframed}[innertopmargin=10pt, linecolor={lightgray}, linewidth=2pt, topline=true, roundcorner=8pt]
        \textbf{Title:} \VAR{layer.spatial_meta.title} \newline \newline
        \textbf{Summary:} \VAR{layer.spatial_meta.summary} \newline \newline
        \textbf{Description:} \VAR{layer.spatial_meta.description} \newline \newline
        \textbf{Tags:} \VAR{layer.spatial_meta.tags} \newline \newline
        \textbf{Credits:} \VAR{layer.spatial_meta.credits}
      \end{mdframed}
    \fi
    \ifnum\VAR{layer.has_sublayers}=1
      \begin{itemize}
        \BLOCK{for lyr in layer.layers}
            \item[--]\nolinkurl{\VAR{lyr}}  (\VAR{layer.data_types[loop.index]})
        \BLOCK{ endfor }
      \end{itemize}
    \fi
    \begin{itemize}
      \item{\textbf{Date Modified:} \VAR{layer.date_modified}}
      \item{\textbf{File Size:} \VAR{layer.filesize}}
    \end{itemize}
  \BLOCK{ endfor }
\BLOCK{ endfor }

\pagebreak

\section{CORRUPT (OR OTHERWISE PROBLEMATIC) LAYERS (\VAR{corrupt_lyrs|length})}
\emph{\color{purple}This section lists any spatial layers that were not handled as expected by the script. One possible 
reason is that the layer is corrupted. This section will be empty if there are no problematic layers.}
\BLOCK{for layer in corrupt_lyrs}
  \begin{itemize}
    \item{\url{\VAR{layer}}}
  \end{itemize}
\BLOCK{ endfor }

\end{document}


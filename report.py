""" Script to generate report of spatial data layers within a specified directory and optionally for all subdirectories.
    Can be executed for a single folder, or recursively on all sub-directories. Report can be output in PDF, CSV, or TXT
    format, or all three.
"""

import argparse
import report_utils
import os


def main(inargs):
    """ Run the program with inargs passed to process function. """
    report_utils.process(inargs.in_path, inargs.out_path, inargs.format, inargs.recursive)


if __name__ == "__main__":
    description = "Generate report of spatial data layers within a specified directory and optionally for all subdirectories."
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("in_path", type=str,
                        help="Absolute path to directory in which to process spatial data layers.")
    parser.add_argument("out_path", type=str,
                        help="Absolute path to directory in which to save output file(s).")
    parser.add_argument("format", type=str,
                        choices=["pdf", "txt", "csv", "all"],
                        help="Output format of report.")
    parser.add_argument("-r", "--recursive", action="store_true",
                        help="Run recursively on all subdirectories.")
    args = parser.parse_args()
    assert (os.path.exists(args.in_path)), f"{args.in_path} does not exist on filesystem."
    assert (os.path.exists(args.out_path)), f"{args.out_path} does not exist on filesystem."
    main(args)

""" Helper functions for generating report of spatial data layers. """

import os
from datetime import date, datetime
import settings
import subprocess
from pathlib import Path
import arcpy
from arcpy import metadata as md
import re
import logging


CORRUPT_LAYERS = []

# https://stackoverflow.com/questions/34954373/disable-format-for-some-messages
class ConditionalFormatter(logging.Formatter):
    def format(self, record):
        if hasattr(record, 'simple') and record.simple:
            return record.getMessage()
        else:
            return logging.Formatter.format(self, record)


def setup_logger(directory, level=logging.INFO):
    """Gets or creates a logger.

    Args:
        directory (str): Directory path for output file
        name (str, optional): Name for logfile. Defaults to LOG_NAME.
        date (datetime, optional): Date to append to logfile name. Defaults to DATE_TIME.
        level (str, optional): Logging level for file. Defaults to logging.INFO.

    Returns:
        logger_obj: Logger object.
    """
    # Gets or creates a logger
    logger_obj = logging.getLogger(settings.LOG_NAME)

    # set log level
    logger_obj.setLevel(level)

    # define file handler and set formatter
    file_handler = logging.FileHandler(os.path.join(directory, "{}-{}.log").format(settings.LOG_NAME, settings.DATE_TIME))
    formatter = ConditionalFormatter("%(asctime)s:%(levelname)s:%(module)s(%(lineno)d) - %(message)s")
    file_handler.setFormatter(formatter)

    # add file handler to logger
    logger_obj.addHandler(file_handler)
    return logger_obj


class FileMetadata:
    def __init__(self, filename, filepath, rootpath):
        self.name = filename
        self.date_modified = get_date_modified(filepath)
        self.filesize = get_filesize(filepath)
        self.has_sublayers = 0
        self.number_layers = 1
        self.layers = []
        self.data_types = self.get_data_type(filepath)
        self.data_type_count = get_type_count(self)
        self.has_spatial_meta = 0
        self.spatial_meta = get_spatial_metadata(filepath)
        self.relative_path = os.path.relpath(filepath, rootpath)

        if filename.split(".")[-1] in settings.NESTED_FORMATS:
            self.layers = get_sublayers(filepath)
            self.has_sublayers = 1
            # Increment the number_layers instance variable by number of sublayers - 1 because of container (.gdb, .gpkg), 
            self.number_layers += len(self.layers) - 1

        if has_spatial_metadata(self.spatial_meta):
            self.has_spatial_meta = 1
    
    def get_data_type(self, filepath):
            """ Return a list of data types from a geoprocessing describe data object 
                for each layer from filepath.
            """
            def helper(describe_object):
                if describe_object.dataType in ["FeatureClass", "ShapeFile"]:
                    return describe_object.shapeType
                elif describe_object.dataType == "RasterDataset":
                    return describe_object.dataType
                elif describe_object.dataType == "Workspace":
                    return describe_object.dataType
                else:
                    return "Other"
            desc = None;        
            try:
                desc = arcpy.Describe(filepath)
            except OSError as e:
                CORRUPT_LAYERS.append(Path(filepath)).as_posix()
                self.number_layers -= 1
                print(e)
            if desc:
                if desc.dataType == "Workspace":
                    arcpy.env.workspace = filepath
                    # Use the ListFeatureClasses function to return a list of
                    lyrs = arcpy.ListFeatureClasses()
                    lyrs += arcpy.ListRasters()
                    formats = [desc.dataType]
                    for l in lyrs:
                        lyr_desc = None
                        try:
                            lyr_desc = arcpy.Describe(l)
                        except OSError as e:
                            CORRUPT_LAYERS.append(Path(os.path.join(filepath, l)).as_posix())
                            self.number_layers -= 1
                            print(e)
                            continue
                        formats.append(helper(lyr_desc))
                    return formats
                return [helper(desc)]


def get_type_count(metadata_object):
    count_dict = {}
    if metadata_object.data_types:
        count_dict["points"] = metadata_object.data_types.count("Point")
        count_dict["multipoints"] = metadata_object.data_types.count("Multipoint")
        count_dict["lines"] = metadata_object.data_types.count("Polyline")
        count_dict["polygons"] = metadata_object.data_types.count("Polygon")
        count_dict["rasters"] = metadata_object.data_types.count("RasterDataset")
    return count_dict


def has_spatial_metadata(metadata_object):
    """ Returns whether metadata has relevant metadata. """
    for field in settings.METADATA_FIELDS:
        if getattr(metadata_object, field) is not None:
            return True
    return False


def get_spatial_metadata(filepath):
    """ If available, get metadata from top level file (i.e., from a shapefile,
        a parent Geodatabase or Geopackage, not individual feature classes.
        Return arcpy.metadata.Metadata object. Cleans string if not None.
    """
    metadata_object = md.Metadata(filepath)
    for field in settings.METADATA_FIELDS:
        if getattr(metadata_object, field) is not None:
            cleaned = clean_string(getattr(metadata_object, field))
            stripped = strip_text(cleaned)
            setattr(metadata_object, field, stripped)
    return metadata_object


def replace_all(text, dict):
    """ For each item in dict, search the key in the text string and replace with the corresponding dict value.
    :param text: string of text
    :param dict: dictionary with key (string to be replaced) and value (string to replace current string with)
    :return: modified string with replaced text
    https://stackoverflow.com/questions/6116978/how-to-replace-multiple-substrings-of-a-string
    """
    for i, j in dict.items():
        text = text.replace(i, j)
    return text


def strip_text(string_to_strip):
    """ Returns a stripped version of STRING_TO_STRIP, after removing all text
        starting at strings in settings.REMOVE_AFTER list.
        >>> strip_text("I love horror movies. #Fra J'aime les films d'horreur.", "#Fra")
        'I love horror movies. '
    """
    for remove_string in settings.REMOVE_AFTER:
        string_to_strip = string_to_strip.split(remove_string)[0]
    return string_to_strip


def get_sublayers(filepath):
    """ Get layers from File Geodatabase or Geopackage. """
    arcpy.env.workspace = filepath
    lyrs = arcpy.ListFeatureClasses()
    lyrs += arcpy.ListRasters()
    return lyrs


def clean_string(bad_string):
    """ Return a cleaned string. Removes HTML tags and the text contained within.
        https://stackoverflow.com/questions/9662346/python-code-to-remove-html-tags-from-a-string
        >>> clean_string('<DIV>Polyline vector of highwater coastline for trim cell area 103.</DIV>')
        'Polyline vector of highwater coastline for trim cell area 103.'
    """
    modified_str = re.sub(settings.CLEANR, "", bad_string)
    return replace_all(modified_str, settings.CHAR_MAP)


def get_filesize(filepath):
    """ Return filesize in KB or MB, as string rounded to two decimal places. """
    if filepath.split(".")[-1] == "gdb":
        filesize_bytes = sum(d.stat().st_size for d in os.scandir(filepath) if d.is_file())
    else:
        filesize_bytes = os.stat(filepath).st_size
    if filesize_bytes / (1024 * 1024) < 1:
        return str(round(filesize_bytes / 1024, 2)) + " KB"
    return str(round(filesize_bytes / (1024 * 1024), 2)) + " MB"


def get_total_layers(data_dict):
    """ Return dict that contains total number of layers from input DATA_DICT. 
        Also contains entries for count of datasets by data type.
    """
    totals = {"total": 0,
              "point_total": 0,
              "multipoint_total": 0,
              "line_total": 0,
              "polygon_total": 0,
              "raster_total": 0}
    for k, v in data_dict.items():
        for metadata_object in v:
            totals["total"] += metadata_object.number_layers
            totals["point_total"] += metadata_object.data_type_count.get("points")
            totals["multipoint_total"] += metadata_object.data_type_count.get("multipoints")
            totals["line_total"] += metadata_object.data_type_count.get("lines")
            totals["polygon_total"] += metadata_object.data_type_count.get("polygons")
            totals["raster_total"] += metadata_object.data_type_count.get("rasters")
    return totals


def get_date_modified(filepath):
    """ Return string of date and time file last modified in the form
        %Y-%m-%d (e.g., 2022-08-15).
    """
    modified_time = os.path.getmtime(filepath)
    modified_date = date.fromtimestamp(modified_time)
    return modified_date.strftime("%Y-%m-%d")


def get_filepaths(root_dir, logger_object, recursive=False):
    """ Return dictionary where keys are absolute paths
        and values are lists of FileMetadata objects for spatial data files within a
        root directory and (optionally) all subdirectories. Ignores
        hidden directories.
    """
    def update_dict(filename, filepath):
        nonlocal filepath_dict
        file_ext = filename.split(".")[-1].lower()
        if file_ext in settings.SPATIAL_FORMATS:
            parent_dir = Path(os.path.dirname(filepath)).as_posix()
            if file_ext == "gdb":
                filename = os.path.basename(filepath)
            logger_object.info(" + Processing file: {}".format(filename))
            file_obj = FileMetadata(filename, filepath, root_dir)
            if parent_dir not in filepath_dict.keys():
                filepath_dict[parent_dir] = [file_obj]
            else:
                filepath_dict[parent_dir].append(file_obj)
    
    if recursive:
        filepath_dict = {}
        for path, dirs, files in os.walk(root_dir):
            l_string = "Searching for spatial data in: {}".format(path)
            logger_object.info("="*len(l_string))
            logger_object.info(l_string)
            logger_object.info("="*len(l_string))
            # Ignore files in hidden directories like .git.
            files = [f for f in files if not f[0] == "."]
            dirs[:] = [d for d in dirs if not d[0] == "."]
            # Swap GDB from dirs to files list.
            [files.append(d) for d in dirs if d.endswith(".gdb")]
            dirs[:] = [d for d in dirs if not d.endswith(".gdb")]
            for name in files:
                update_dict(name, os.path.join(path, name))
    else:
        filepath_dict = {}
        for name in os.listdir(root_dir):
            update_dict(name, os.path.join(root_dir, name))
    return filepath_dict


def convert_to_latex(tex_template, output_directory, filepath_dict, number_of_layers):
    """ Populate LaTeX template with metadata and return path to output file. """
    template = settings.latex_jinja_env.get_template(tex_template)

    # Variables for generic filename and LaTeX extension.
    filename = f"{settings.DOC_META.get('title').replace(' ', '_')}_{date.today().strftime('%Y-%m-%d')}"
    latex_filename = f"{filename}.tex"

    # Write populated tex file to output directory.
    with open(os.path.join(output_directory, latex_filename), "w") as tex_file:
        tex_file.write(template.render(lheader=settings.DOC_META.get("lheader"),
                                       rheader=settings.DOC_META.get("rheader"),
                                       title=settings.DOC_META.get("title"),
                                       author=settings.DOC_META.get("author"),
                                       date=settings.DOC_META.get("date_text"),
                                       filepath_dictionary=filepath_dict,
                                       num_layers=number_of_layers,
                                       format_list=settings.SPATIAL_FORMATS,
                                       corrupt_lyrs=CORRUPT_LAYERS)
                       )
    return latex_filename


def convert_to_pdf(latex_document, output_directory):
    """ Convert populated LaTeX document to PDF and return filepath to PDF. """
    # https://tex.stackexchange.com/questions/258814/what-is-the-difference-between-interaction-nonstopmode-and-halt-on-error
    # https://latex.org/forum/viewtopic.php?t=26360 --> specify output directory (for pdf, log, and aux files)
    return subprocess.call(["pdflatex",
                            "-output-directory",
                            output_directory,
                            latex_document,
                            "interaction=nonstopmode"])


def process(in_path, out_path, format, recursive=False):
    """ Call helper functions to get data and populate required documents. """
    logger = setup_logger(out_path)
    file_dict = get_filepaths(in_path, logger, recursive=recursive)
    total_layers = get_total_layers(file_dict)
    total_layers["total"] -= len(CORRUPT_LAYERS)
    latex_doc = convert_to_latex(settings.LATEX_TEMPLATE, out_path, file_dict, total_layers)
    pdf_doc = convert_to_pdf(latex_doc, out_path)

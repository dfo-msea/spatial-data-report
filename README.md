# Spatial Data Report

__Main author:__  Cole Fields
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: Cole.Fields@dfo-mpo.gc.ca | tel: 250-363-8060


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
Generate a PDF report of spatial data layers within a specified directory and optionally all sub-directories.


## Summary
The Pacific GIS working group expressed a desire for a tool that would generate a report of spatial data layers 
that were hosted on the regional datastore on a shared network drive. This tool is generic in the sense that it 
will run on a single directory, or recursively walk through the filesystem from a specified directory, looking 
for spatial data layers. To run the program, clone the repo and `cd` into the directory. Run `python report.py -h` for 
help text. For example, running `python report.py D:\projects\gis-data C:\Temp pdf -r` will execute the program and 
recursively walk through folders within the input directory. If the script is successful, a PDF document will be 
written to the output folder specified.

## Status
In-development


## Contents
`template.tex` is a template file that will be populated with file information and metadata. It contains styling and 
some logic for iterating over objects passed as variables.

`settings.py` contains various constants used in `report_utils.py`. The data is kept separate from the scripts that 
contain the logic of the program.

`report_utils.py` contains the logic of the program for walking the filesystem, getting metadata, and preparing the 
data structures to pass to `template.tex`.

`report.py` is the entry point of the program that a user can execute from the command line with specified arguments.


## Methods
What methods were used to achieve the purpose? This should be detailed as possible.
### Subsections within methods
Often useful to organise the methods under multiple subheadings.


## Requirements
Requires python3.6+, LaTeX, and python environment with ArcPy module used for getting spatial data metadata. 
ArcGIS Pro will likely have python executable installed here: C:\Program Files\ArcGIS\Pro\bin\Python\envs\arcgispro-py3.
Opening the `Python Command Prompt` should automatically activate the `arcgispro-py3` virtual environment that
can be used to run the script.

## Caveats
* Currently, only the `pdf` option for the `formats` parameter is supported. Some characters are modified from their 
original encoding due to LaTeX formatting and problems encountered with special characters for formatting. 
See `settings.CHAR_MAP` for mappings.
* Removes text after `#Fra` if that exists in the metadata. This is the French translated text for some layers that are 
from Open Maps.

## Uncertainty
* Only considers the following file formats as spatial data: `gdb gpkg shp tif tiff geotiff`.
* Does not generally support text with accents such as `è é`.

## Acknowledgements
*Optional section.* List any contributors and acknowledge relevant people or institutions


## References
*Optional section.*

""" File containing various constants to be used in REPORT_UTILS. 
"""

import getpass
import os
import jinja2
from datetime import date, datetime
import re

NESTED_FORMATS = ["gdb", "gpkg"]
VECTOR_FORMATS = NESTED_FORMATS[:] + ["shp"]
RASTER_FORMATS = ["tif", "tiff", "geotiff"]
SPATIAL_FORMATS = VECTOR_FORMATS + RASTER_FORMATS
METADATA_FIELDS = ["credits", "description", "summary", "tags", "title"]
USER = getpass.getuser()
REF_FOLDER = os.path.join(os.getcwd(), "reference")
LATEX_TEMPLATE = "template.tex"
TODAY = date.today()
CLEANR = re.compile("<.*?>")
REMOVE_AFTER = ["#Fra", "#fra"]
DATE_TIME = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
LOG_NAME = "spatial-data-report"

DOC_META = {"title": "SPATIAL DATA REPORT",
            "lheader": "FISHERIES AND OCEANS CANADA",
            "rheader": "SPATIAL DATA REPORT",
            "date_text": TODAY.strftime("%B %d, %Y"),
            "author": USER}

# Jinja2 env.
latex_jinja_env = jinja2.Environment(
    block_start_string="\BLOCK{",
    block_end_string="}",
    variable_start_string="\VAR{",
    variable_end_string="}",
    comment_start_string="\#{",
    comment_end_string="}",
    line_statement_prefix="%%",
    line_comment_prefix="%#",
    trim_blocks=True,
    autoescape=False,
    loader=jinja2.FileSystemLoader(REF_FOLDER)
)

# Characters to map to (LaTeX formatting).
# https://www.andy-roberts.net/writing/latex/formatting
# https://www.stevesque.com/symbols/
CHAR_MAP = {"_": " ",
            "’": "'",
            "‘": "'",
            "`": "'",
            "”": "'",
            "“": "'",
            '"': "'",
            "\\": "/",
            "\r\n": u"\u000A",
            "&": "and",
            "–": "--",
            "─": "--",
            "—": "--",
            "−": "-",
            "# ": "",
            "##": "",
            "###": "",
            "####": "",
            "#####": "",
            "%": r"\%",
            "^": r"\^{}",
            "#": r"\#",
            "≥": "$\geq$",
            ">": "$>$",
            "≤": "$\geq$",
            "<": "$<$",
            "±": "$\pm$",
            "×": r"$\times$",
            "÷": r"$\div$",
            "∼": r"$\sim$",
            "˜": r"$\sim$",
            "ñ": r"\~n",
            "°": "$^{\circ}$",
            "◦": "$^{\circ}$",
            "ÿ": "",
            "•": "",
            "µ": r"$\mu$",
            "π": "$\pi$",
            u"é": "e",
            u"è": "e",
            "²": "$^2$",
            "ú": "u",
            "á": "a",
            "í": "i"}
